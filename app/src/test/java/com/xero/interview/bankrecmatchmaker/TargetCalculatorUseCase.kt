package com.xero.interview.bankrecmatchmaker

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.domain.repository.MatchRepository
import com.xero.interview.bankrecmatchmaker.domain.usescases.TargetCalculatorUseCase
import com.xero.interview.bankrecmatchmaker.repository.MatchDataRepository
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.mockito.Mock
import org.mockito.Mockito

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class TargetCalculatorUseCase {

    private lateinit var repository:MatchRepository
    private lateinit var targetCalculatorUseCase: TargetCalculatorUseCase


    private var matchItemMock = MatchItem("City Limousines", "30 Aug", 249.00f, "Sales Invoice")

    private var matchItemList  = arrayListOf(MatchItem("City Limousines", "30 Aug", 249.00f, "Sales Invoice"),
        MatchItem("Ridgeway University", "12 Sep", 618.50f, "Sales Invoice"))

    @Before
    fun setup(){
        repository = Mockito.mock(MatchRepository::class.java)
        targetCalculatorUseCase =  Mockito.mock(TargetCalculatorUseCase::class.java)
    }


    @Test
    fun testSubtraction_userSelectAnInvoice() {

        Mockito.`when`(repository.getTarget()).thenReturn(10000.00f)
        val targetValue = repository.getTarget()

        val subtractionValue = targetValue - matchItemMock.total
        Mockito.`when`(targetCalculatorUseCase.processSubtraction(matchItemMock)).thenReturn(subtractionValue)
        val result = targetCalculatorUseCase.processSubtraction(matchItemMock)

        Mockito.verify(repository).getTarget()
        Mockito.verify(targetCalculatorUseCase).processSubtraction(matchItemMock)
        assertEquals(result, targetValue - matchItemMock.total)
    }

    @Test
    fun testAddition_userUnSelectAnInvoice() {

        Mockito.`when`(repository.getTarget()).thenReturn(10000.00f)
        val targetValue = repository.getTarget()

        val subtractionValue = targetValue - matchItemMock.total
        Mockito.`when`(targetCalculatorUseCase.processSubtraction(matchItemMock)).thenReturn(subtractionValue)
        val result = targetCalculatorUseCase.processSubtraction(matchItemMock)

        val additionValue = result + matchItemMock.total
        Mockito.`when`(targetCalculatorUseCase.processAddition(matchItemMock)).thenReturn(additionValue)
        val resultAdd = targetCalculatorUseCase.processAddition(matchItemMock)

        Mockito.verify(repository).getTarget()
        Mockito.verify(targetCalculatorUseCase).processSubtraction(matchItemMock)
        Mockito.verify(targetCalculatorUseCase).processAddition(matchItemMock)
        assertEquals(resultAdd  , targetValue)
    }

    @Test
    fun testProcessAutomaticMatch_WhenTargetIsBiggerThanTotalInvoice(){
        var invoiceMatchValue = 0.0f

        Mockito.`when`(targetCalculatorUseCase.processAutomaticMatch()).thenReturn(matchItemList)
        val resultList = targetCalculatorUseCase.processAutomaticMatch()

        for (itemMatch in resultList){
            invoiceMatchValue += itemMatch.total
        }

        Mockito.verify(targetCalculatorUseCase).processAutomaticMatch()
        assertEquals(867.5f, invoiceMatchValue)

    }


}
