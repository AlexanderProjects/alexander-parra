package com.xero.interview.bankrecmatchmaker.repository.local

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem

object LocalData {

    var matchItemList  = arrayListOf(MatchItem("City Limousines", "30 Aug", 249.00f, "Sales Invoice"),
        MatchItem("Ridgeway University", "12 Sep", 618.50f, "Sales Invoice"),
        MatchItem("Cube Land", "22 Sep", 495.00f, "Sales Invoice"),
        MatchItem("Bayside Club", "23 Sep", 234.00f, "Sales Invoice"),
        MatchItem("SMART Agency", "12 Sep", 250f, "Sales Invoice"),
        MatchItem("PowerDirect", "11 Sep", 108.60f, "Sales Invoice"),
        MatchItem("PC Complete", "17 Sep", 216.99f, "Sales Invoice"),
        MatchItem("Truxton Properties", "17 Sep", 181.25f, "Sales Invoice"),
        MatchItem("MCO Cleaning Services", "17 Sep", 170.50f, "Sales Invoice"),
        MatchItem("Gateway Motors", "18 Sep", 411.35f, "Sales Invoice"))

    //2935.1902f
    //358.60f
    //867.5f
    var target = 10000.00f
}