package com.xero.interview.bankrecmatchmaker.repository

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.domain.repository.MatchRepository
import com.xero.interview.bankrecmatchmaker.repository.local.LocalData

class MatchDataRepository : MatchRepository {

    override fun getTarget(): Float = LocalData.target

    override fun loadData(): List<MatchItem> = LocalData.matchItemList

}