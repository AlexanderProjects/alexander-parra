package com.xero.interview.bankrecmatchmaker.presentation.CustomComponents

import android.content.Context
import android.support.v7.widget.AppCompatCheckBox
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Checkable
import android.widget.LinearLayout
import com.xero.interview.bankrecmatchmaker.R

class CheckedListItem : LinearLayout, Checkable {

    private var checkBox: AppCompatCheckBox? = null
    private var listener: OnCheckedChangeListener? = null


    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        val layoutInflater = LayoutInflater.from(context)
        orientation = LinearLayout.HORIZONTAL
        checkBox = layoutInflater.inflate(R.layout.list_item_checkbox, this, false) as AppCompatCheckBox
        addView(checkBox, 0)
        setOnClickListener { view ->
            checkBox!!.toggle()
            listener?.onCheckedChange(view, isChecked)
        }
    }

    override fun setChecked(checked: Boolean) {
        checkBox!!.isChecked = checked
    }

    override fun isChecked(): Boolean {
        return checkBox!!.isChecked
    }

    override fun toggle() {
        checkBox!!.toggle()
    }

    fun setOnCheckedChangeListener(listener: OnCheckedChangeListener) {
        this.listener = listener
    }

}