package com.xero.interview.bankrecmatchmaker.presentation.Adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.xero.interview.bankrecmatchmaker.R
import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.presentation.CustomComponents.CheckedListItem
import com.xero.interview.bankrecmatchmaker.presentation.CustomComponents.OnCheckedChangeListener
import java.util.*
import kotlin.concurrent.timerTask


class MatchAdapter(private var matchItemList:List<MatchItem>) : RecyclerView.Adapter<MatchAdapter.ViewHolder>() {

    private lateinit var onItemClickListener:OnItemClickListener
    private var isCheckBoxEnable : Boolean = false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(R.layout.list_item_match, parent, false) as CheckedListItem

        var viewHolder = ViewHolder(itemView)


        itemView.setOnCheckedChangeListener(object : OnCheckedChangeListener {
            override fun onCheckedChange(checkableView: View, isChecked: Boolean) {

                var matchItem = matchItemList[viewHolder.adapterPosition]
                if(isChecked){
                    onItemClickListener.onCheckItem(matchItem)
                }else{
                    onItemClickListener.onUnCheckItem(matchItem)
                }
            }
        })

        if (isCheckBoxEnable)
            itemView.isChecked = isCheckBoxEnable

        return viewHolder
    }

    /**
     * This method update a selected item in the list
     */

    fun selectedInvoice(matchItem: MatchItem) {
        notifyItemChanged(matchItemList.indexOf(matchItem))
    }
    /**
     * This method update the match list with selected items
     */

    fun selectListOfInvoices(itemList: List<MatchItem>){

        Timer().schedule(timerTask {
            for(matchItem in matchItemList) {
                for (item in itemList) {
                    if (item == matchItem) {
                        notifyItemChanged(matchItemList.indexOf(item))
                        isCheckBoxEnable = true
                    }
                }
            }
        }, 1000)

    }


    /**
     * This method set the checkbox callback
     */
    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }

    override fun getItemCount(): Int = matchItemList.count()


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(matchItemList[position])
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mainText: TextView = itemView.findViewById(R.id.text_main)
        private val total: TextView = itemView.findViewById(R.id.text_total)
        private val subtextLeft: TextView = itemView.findViewById(R.id.text_sub_left)
        private val subtextRight: TextView = itemView.findViewById(R.id.text_sub_right)

        fun bind(matchItem: MatchItem) {
            mainText.text = matchItem.paidTo
            total.text = matchItem.total.toString()
            subtextLeft.text = matchItem.transactionDate
            subtextRight.text = matchItem.docType
        }

    }


}