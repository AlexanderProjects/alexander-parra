package com.xero.interview.bankrecmatchmaker

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.xero.interview.bankrecmatchmaker.di.components.ApplicationComponent
import com.xero.interview.bankrecmatchmaker.di.components.DaggerApplicationComponent
import com.xero.interview.bankrecmatchmaker.di.modules.ApplicationModule
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject

class bankrecMatchmakerApplication : Application(), HasActivityInjector{

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Activity>


    lateinit var component : ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .build()
        component.inject(this)

    }

    override fun activityInjector(): AndroidInjector<Activity> = activityInjector



}