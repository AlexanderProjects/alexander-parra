package com.xero.interview.bankrecmatchmaker.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.xero.interview.bankrecmatchmaker.di.factory.ViewModelFactory
import com.xero.interview.bankrecmatchmaker.di.scope.ViewModelKey
import com.xero.interview.bankrecmatchmaker.presentation.Home.HomeMatchViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeMatchViewModel::class)
    internal abstract fun bindAbacusViewModel(homeMatchViewModel: HomeMatchViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}