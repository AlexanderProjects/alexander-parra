package com.xero.interview.bankrecmatchmaker.domain.repository

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem

interface MatchRepository {

    fun loadData(): List<MatchItem>
    fun getTarget():Float

}