package com.xero.interview.bankrecmatchmaker.presentation.Home

import android.arch.lifecycle.ViewModel
import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.domain.usescases.LoadDataUseCase
import com.xero.interview.bankrecmatchmaker.domain.usescases.TargetCalculatorUseCase
import javax.inject.Inject

class HomeMatchViewModel @Inject constructor (private val loadDataUseCase:LoadDataUseCase,
                                              private val targetCalculatorUseCase: TargetCalculatorUseCase): ViewModel() {

    private lateinit var view:HomeMatchView

    fun injectView(view: HomeMatchView){
        this.view = view
    }

    fun loadMatchData(): List<MatchItem> = loadDataUseCase.getData()

    fun getTarget(): Float = loadDataUseCase.getTarget()

    fun getTargetResult ():Float = targetCalculatorUseCase.targetValueUpdated

    fun selectCheckBox(matchItem: MatchItem) = view.updateTarget(targetCalculatorUseCase.processSubtraction(matchItem))

    fun unSelectCheckBox(matchItem: MatchItem) =  view.updateTarget(targetCalculatorUseCase.processAddition(matchItem))

    fun processAutoSelectMatchItems() = view.autoSelectMatchItems(targetCalculatorUseCase.processAutomaticMatch())

}