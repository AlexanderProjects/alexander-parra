package com.xero.interview.bankrecmatchmaker.presentation.Home

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem

interface HomeMatchView {

    fun updateTarget(target: Float)

    fun updateValues(matchItem: MatchItem)

    fun autoSelectMatchItems(matchItemList:List<MatchItem>)

}