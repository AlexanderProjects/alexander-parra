package com.xero.interview.bankrecmatchmaker.domain.models

data class MatchItem (var paidTo:String,
                      var transactionDate:String,
                      var total:Float,
                      var docType:String)