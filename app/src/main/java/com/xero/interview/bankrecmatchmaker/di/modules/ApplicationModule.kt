package com.xero.interview.bankrecmatchmaker.di.modules

import android.app.Application
import com.xero.interview.bankrecmatchmaker.bankrecMatchmakerApplication
import com.xero.interview.bankrecmatchmaker.domain.repository.MatchRepository
import com.xero.interview.bankrecmatchmaker.repository.MatchDataRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class ApplicationModule (private val bankrecMatchmakerApplication: bankrecMatchmakerApplication) {

    @Provides
    @Singleton
    fun provideApplication(): Application = bankrecMatchmakerApplication!!

    @Provides
    @Singleton
    fun provideRepository(): MatchRepository = MatchDataRepository()

}