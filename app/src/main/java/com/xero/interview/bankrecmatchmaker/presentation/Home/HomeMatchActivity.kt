package com.xero.interview.bankrecmatchmaker.presentation.Home

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.TextView
import com.xero.interview.bankrecmatchmaker.R
import com.xero.interview.bankrecmatchmaker.bankrecMatchmakerApplication
import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.presentation.Adapters.MatchAdapter
import com.xero.interview.bankrecmatchmaker.presentation.Adapters.OnItemClickListener
import javax.inject.Inject

class HomeMatchActivity : AppCompatActivity(), HomeMatchView {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var viewModel: HomeMatchViewModel

    private lateinit var adapter: MatchAdapter
    private lateinit var matchText: TextView
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        (application as bankrecMatchmakerApplication).component.inject(this)
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(HomeMatchViewModel::class.java)
        viewModel.injectView(this)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        matchText = findViewById(R.id.match_text)
        recyclerView = findViewById(R.id.recycler_view)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setTitle(R.string.title_find_match)

        matchText.text = getString(R.string.select_matches, viewModel.getTarget())

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = MatchAdapter(viewModel.loadMatchData())
        adapter.setOnItemClickListener(object : OnItemClickListener {

            override fun onCheckItem(item: MatchItem) {
                viewModel.selectCheckBox(item)
            }
            override fun onUnCheckItem(item: MatchItem) {
               viewModel.unSelectCheckBox(item)
            }

        })
        recyclerView.adapter = adapter
        viewModel.processAutoSelectMatchItems()
    }

    override fun updateTarget(target:Float){
        matchText.text = getString(R.string.select_matches, target)
    }

    override fun updateValues(matchItem: MatchItem) {
        adapter.selectedInvoice(matchItem)
    }

    override fun autoSelectMatchItems(matchItemList: List<MatchItem>) {
        adapter.selectListOfInvoices(matchItemList)
        updateTarget(viewModel.getTargetResult())
    }

}
