package com.xero.interview.bankrecmatchmaker.presentation.Adapters

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem

interface OnItemClickListener {
    fun onCheckItem(item : MatchItem)
    fun onUnCheckItem(item: MatchItem)
}