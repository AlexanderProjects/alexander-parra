package com.xero.interview.bankrecmatchmaker.domain.usescases

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.domain.repository.MatchRepository
import javax.inject.Inject

class LoadDataUseCase @Inject constructor (private val repository: MatchRepository)  {

    fun getData() : List<MatchItem> = repository.loadData()

    fun getTarget() : Float = repository.getTarget()
}