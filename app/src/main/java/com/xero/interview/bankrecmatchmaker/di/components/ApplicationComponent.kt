package com.xero.interview.bankrecmatchmaker.di.components

import com.xero.interview.bankrecmatchmaker.bankrecMatchmakerApplication
import com.xero.interview.bankrecmatchmaker.di.modules.ApplicationModule
import com.xero.interview.bankrecmatchmaker.di.modules.ViewModelModule
import com.xero.interview.bankrecmatchmaker.presentation.Home.HomeMatchActivity
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidInjectionModule::class, ViewModelModule::class, ApplicationModule::class])
interface ApplicationComponent {

    fun inject(bankrecMatchmakerApplication: bankrecMatchmakerApplication)
    fun inject(homeMatchActivity: HomeMatchActivity)
}