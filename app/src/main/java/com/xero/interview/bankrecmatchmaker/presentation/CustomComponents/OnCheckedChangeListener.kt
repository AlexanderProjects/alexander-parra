package com.xero.interview.bankrecmatchmaker.presentation.CustomComponents

import android.view.View

interface OnCheckedChangeListener {
    fun onCheckedChange(checkableView: View,isChecked : Boolean)
}