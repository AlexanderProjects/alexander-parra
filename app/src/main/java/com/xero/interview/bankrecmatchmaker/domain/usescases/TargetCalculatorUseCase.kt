package com.xero.interview.bankrecmatchmaker.domain.usescases

import com.xero.interview.bankrecmatchmaker.domain.models.MatchItem
import com.xero.interview.bankrecmatchmaker.domain.repository.MatchRepository
import java.util.ArrayList
import javax.inject.Inject

class TargetCalculatorUseCase @Inject constructor(private val repository: MatchRepository)  {

    private var totalInvoices : Float = 0.0f
    var targetValueUpdated: Float = repository.getTarget()
    private var remainList:ArrayList<MatchItem> = ArrayList()

    fun processSubtraction(matchItem:MatchItem) : Float {
        totalInvoices = if (totalInvoices == 0.0f) repository.getTarget() else totalInvoices
        totalInvoices -= matchItem.total
        targetValueUpdated = totalInvoices
        return targetValueUpdated
    }

    fun processAddition(matchItem: MatchItem) : Float {
        totalInvoices += matchItem.total
        targetValueUpdated = totalInvoices
        return targetValueUpdated
    }


    fun processAutomaticMatch(): List<MatchItem> {
        var total = 0.0f
        repository.loadData().forEach {
            total += it.total
        }

        if(total < repository.getTarget()){
            targetValueUpdated = repository.getTarget() - total
            totalInvoices = targetValueUpdated
            remainList = repository.loadData() as ArrayList<MatchItem>
        } else {
            calculateCombinationWhenTotalInvoicesMinorTarget(repository.loadData() as ArrayList<MatchItem>,repository.getTarget(), ArrayList())
        }

        return remainList
    }


    private fun calculateCombinationWhenTotalInvoicesMinorTarget(dataList:ArrayList<MatchItem> ,target:Float, partial:ArrayList<MatchItem>){
        var result = 0.0f

        partial.forEach {
            result += it.total
        }

        if (result == target){
            remainList = partial
            targetValueUpdated = target - result
        }

        if (result >= target){
            return
        }

        for (i in 0 until dataList.count()){
            val remainingList = ArrayList<MatchItem>()
            val item = dataList[i]
            for (j in i + 1 until dataList.count()){
                remainingList.add(dataList[j])
            }
            val part = ArrayList<MatchItem>(partial)
            part.add(item)
            calculateCombinationWhenTotalInvoicesMinorTarget(remainingList,target,part)
        }

    }

}